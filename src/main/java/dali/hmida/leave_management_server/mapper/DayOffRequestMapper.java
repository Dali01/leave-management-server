package dali.hmida.leave_management_server.mapper;

import dali.hmida.leave_management_server.dto.DayOffRequestDTO;
import dali.hmida.leave_management_server.model.DayOffRequest;
import lombok.SneakyThrows;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

@Mapper(componentModel = "spring")
public abstract class DayOffRequestMapper {

  public static final DayOffRequestMapper MAPPER = Mappers.getMapper(DayOffRequestMapper.class);
  SimpleDateFormat targetFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

  SimpleDateFormat originalFormatter =
      new SimpleDateFormat("MMM dd, yyyy HH:mm:ss", Locale.ENGLISH);

  @Mapping(target = "startDate", ignore = true)
  @Mapping(target = "endDate", ignore = true)
  public abstract DayOffRequest fromDto(DayOffRequestDTO dayOffRequestDTO);

  @Mapping(target = "startDate", ignore = true)
  @Mapping(target = "endDate", ignore = true)
  public abstract DayOffRequestDTO toDto(DayOffRequest dayOffRequest);

  @AfterMapping
  @SneakyThrows
  void updateDayOffRequest(
      final DayOffRequestDTO dayOffRequestDTO, @MappingTarget final DayOffRequest dayOffRequest) {
    originalFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

    dayOffRequest.setStartDate(originalFormatter.parse(dayOffRequestDTO.getStartDate()));
    dayOffRequest.setEndDate(originalFormatter.parse(dayOffRequestDTO.getEndDate()));
  }

  @AfterMapping
  void updateDayOffRequestDTO(
      final DayOffRequest dayOffRequest, @MappingTarget final DayOffRequestDTO dayOffRequestDTO) {
    targetFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
    dayOffRequestDTO.setStartDate(targetFormatter.format(dayOffRequest.getStartDate()));
    dayOffRequestDTO.setEndDate(targetFormatter.format(dayOffRequest.getEndDate()));
  }
}
