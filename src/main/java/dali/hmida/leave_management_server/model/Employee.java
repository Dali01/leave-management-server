package dali.hmida.leave_management_server.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Employee {
  @Id private String id;
  private String email;
  private String username;
  private String department;
  private String type;
  private String location;
  private Integer phoneNumber;
  private String password;

  public Employee(
      final String email,
      final String username,
      final String department,
      final String type,
      final String location,
      final Integer phoneNumber,
      final String password) {
    this.email = email;
    this.username = username;
    this.department = department;
    this.type = type;
    this.location = location;
    this.phoneNumber = phoneNumber;
    this.password = password;
  }
}
