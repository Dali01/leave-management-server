package dali.hmida.leave_management_server.service;

import dali.hmida.leave_management_server.model.DayOffRequest;
import dali.hmida.leave_management_server.model.Employee;
import dali.hmida.leave_management_server.repository.DayOffRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class DayOffRequestService {

  private final DayOffRequestRepository dayOffRequestRepository;
  private final EmployeeService employeeService;

  @Autowired
  public DayOffRequestService(
      final DayOffRequestRepository dayOffRequestRepository,
      final EmployeeService employeeService) {
    this.dayOffRequestRepository = dayOffRequestRepository;
    this.employeeService = employeeService;
  }

  public DayOffRequest saveDayOffRequest(final DayOffRequest dayOffRequest) {
    return dayOffRequestRepository.save(dayOffRequest);
  }

  public List<DayOffRequest> getBetween(final Date start, final Date end, final String department) {

    final ArrayList<DayOffRequest> newList = new ArrayList<>();
    final List<DayOffRequest> listt =
        dayOffRequestRepository.findByStartDateIsLessThanAndEndDateIsGreaterThan(start, end);
    for (final DayOffRequest dayOffRequest : listt) {
      final Employee employee = employeeService.getEmployeeByName(dayOffRequest.getRequester());
      if (employee != null && Objects.equals(employee.getDepartment(), department)) {
        newList.add(dayOffRequest);
      }
    }
    return newList;
  }

  public List<DayOffRequest> getByRequester(final String name) {
    return dayOffRequestRepository.findByRequesterEquals(name);
  }

  // ma sta3melthech fonction 5ater badalneha bel fou9aniya

  //    public ArrayList<DayOffRequest> getDayOffRequests(String date) throws ParseException {
  //        int i;
  ////        ,day,year;
  ////        String month;
  ////        String[] substrings = date.split(",");
  ////        day = Integer.parseInt(substrings[0]) ;
  ////        month = substrings[1];
  ////        year = Integer.parseInt(substrings[2]);
  //
  //        SimpleDateFormat sdf = new SimpleDateFormat("dd, MMMM, yyyy", Locale.ENGLISH);
  //        Date selectedDate = sdf.parse(date);
  //
  //        List<DayOffRequest> list = dayOffRequestRepository.findAll();
  //        ArrayList<DayOffRequest> array = new ArrayList<>();
  //        for (i = 0; i < list.size() ; i++)
  //        {
  //            //convert start date and end date
  //            Date startDate = list.get(i).getStartDate();
  //            Date endDate = list.get(i).getEndDate();
  //            if (selectedDate.compareTo(startDate) == 0 || selectedDate.compareTo(endDate) == 0
  // || (selectedDate.compareTo(startDate)>0 && selectedDate.compareTo(endDate)<0)){
  //                array.add(list.get(i));
  //            }
  //        }
  //        return array;
  //    }
  public void deleteAllDayOffRequests() {
    dayOffRequestRepository.deleteAll();
  }

  public List<DayOffRequest> getPendingDayOffs() {
    final List<DayOffRequest> dayOffs = dayOffRequestRepository.findAll();
    final List<DayOffRequest> pendingDayOffs =
        dayOffs.stream()
            .filter(d -> Objects.equals(d.getStatus(), "Pending"))
            .collect(Collectors.toList());
    return pendingDayOffs;
  }

  public List<DayOffRequest> getDayOffsHistory() {
    final List<DayOffRequest> dayOffs = dayOffRequestRepository.findAll();
    final List<DayOffRequest> DayOffsHistory =
        dayOffs.stream()
            .filter(d -> !Objects.equals(d.getStatus(), "Pending"))
            .collect(Collectors.toList());
    return DayOffsHistory;
  }

  public List<DayOffRequest> getAllDayOffs() {
    return dayOffRequestRepository.findAll();
  }
}
