package dali.hmida.leave_management_server;

import dali.hmida.leave_management_server.model.DayOffRequest;
import dali.hmida.leave_management_server.model.Employee;
import dali.hmida.leave_management_server.model.Type;
import dali.hmida.leave_management_server.service.DayOffRequestService;
import dali.hmida.leave_management_server.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@SpringBootApplication
public class LeaveManagementServerApplication {

  private static EmployeeService employeeService;
  private static DayOffRequestService dayOffRequestService;

  @Autowired
  public LeaveManagementServerApplication(
      final EmployeeService employeeService, final DayOffRequestService dayOffRequestService) {
    this.employeeService = employeeService;
    this.dayOffRequestService = dayOffRequestService;
  }

  public static void main(final String[] args) {
    SpringApplication.run(LeaveManagementServerApplication.class, args);
    log();

    //		Logger logger = LoggerFactory.getLogger(LeaveManagementServerApplication.class);
    //		logger.error("daaa");
  }

  public static void log() {
    employeeService.deleteAllEmployees();
    dayOffRequestService.deleteAllDayOffRequests();

    // saving to employees in database
    final Employee emp1 =
        new Employee(
            "mohamedali.hmida@isimg.tn",
            "dali",
            "CS",
            Type.MANAGER.toString(),
            "Bennane",
            55683657,
            "Dali123");
    final Employee emp2 =
        new Employee(
            "mehdi.hmida@isimg.tn",
            "mehdi",
            "Customer Service",
            Type.EMPLOYEE.toString(),
            "bennane",
            99664553,
            "Mehdi147");
    final Employee emp3 =
        new Employee(
            "saied.hmida@isimg.tn",
            "Saied",
            "Customer Service",
            Type.EMPLOYEE.toString(),
            "bennane",
            99664555,
            "Saied123");
    final Employee emp4 =
        new Employee(
            "rachida.hmida@isimg.tn",
            "Rachida",
            "Customer Service",
            Type.EMPLOYEE.toString(),
            "Mourouj",
            99664500,
            "Rachida147");
    final Employee emp5 =
        new Employee(
            "naziha.hmida@isimg.tn",
            "Naziha",
            "CS",
            Type.EMPLOYEE.toString(),
            "Bennan",
            98683657,
            "Naziha147");

    final Employee emp6 =
        new Employee(
            "mohsen.hmida@isimg.tn",
            "Mohsen",
            "CS",
            Type.EMPLOYEE.toString(),
            "Mourouj",
            98683657,
            "Mohsen147");

    final Employee emp7 =
        new Employee(
            "Jasser.hmida@isimg.tn",
            "Jasser",
            "CS",
            Type.EMPLOYEE.toString(),
            "Mourouj",
            98683650,
            "Jasser147");
    employeeService.saveEmployee(emp1);
    employeeService.saveEmployee(emp2);
    employeeService.saveEmployee(emp3);
    employeeService.saveEmployee(emp4);
    employeeService.saveEmployee(emp5);
    employeeService.saveEmployee(emp6);
    employeeService.saveEmployee(emp7);

    final Logger logger = LoggerFactory.getLogger(LeaveManagementServerApplication.class);
    logger.error(emp1.toString());

    // preparing date
    final Date date = new Date();
    final SimpleDateFormat sdf = new SimpleDateFormat("dd, MMMM, yyyy", Locale.ENGLISH);
    final Date selectedDate = new Date(date.getTime());
    final Date selectedDate1 = new Date(date.getTime() + (172800000 * 2));
    final Date selectedDate2 = new Date(date.getTime() + (172800000 * 3));
    final Date selectedDate3 = new Date(date.getTime() + (172800000 * 4));
    final Date selectedDate4 = new Date(date.getTime() + (172800000 * 5));
    final Date selectedDate5 = new Date(date.getTime() + (84600000 * 3));

    // saving two dayOff requests

    final DayOffRequest dayOffRequest3 =
        new DayOffRequest("Saied", 1, "Accepted", "Casual", selectedDate, selectedDate3);
    final DayOffRequest dayOffRequest4 =
        new DayOffRequest("Naziha", 5, "Refused", "Casual", selectedDate, selectedDate1);
    final DayOffRequest dayOffRequest5 =
        new DayOffRequest("Rachida", 5, "Refused", "Illness", selectedDate, selectedDate5);
    final DayOffRequest dayOffRequest6 =
        new DayOffRequest("dali", 1, "Accepted", "Casual", selectedDate, selectedDate2);
    final DayOffRequest dayOffRequest7 =
        new DayOffRequest("Mohsen", 4, "Accepted", "Illness", selectedDate, selectedDate5);
    final DayOffRequest dayOffRequest8 =
        new DayOffRequest("Jasser", 1, "Accepted", "Casual", selectedDate, selectedDate2);

    dayOffRequestService.saveDayOffRequest(dayOffRequest3);
    dayOffRequestService.saveDayOffRequest(dayOffRequest4);
    dayOffRequestService.saveDayOffRequest(dayOffRequest5);
    dayOffRequestService.saveDayOffRequest(dayOffRequest6);
    dayOffRequestService.saveDayOffRequest(dayOffRequest7);
    dayOffRequestService.saveDayOffRequest(dayOffRequest8);
  }
}
