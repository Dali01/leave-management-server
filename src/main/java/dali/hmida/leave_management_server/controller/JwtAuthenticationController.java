package dali.hmida.leave_management_server.controller;

import dali.hmida.leave_management_server.config.JwtTokenUtil;
import dali.hmida.leave_management_server.model.Employee;
import dali.hmida.leave_management_server.model.JwtRequest;
import dali.hmida.leave_management_server.model.Response;
import dali.hmida.leave_management_server.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

  @Autowired private AuthenticationManager authenticationManager;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private PasswordEncoder bCryptPasswordEncoder;

  @Autowired private UserDetailsService jwtInMemoryUserDetailsService;

  @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
  public ResponseEntity<?> createAuthenticationToken(@RequestBody final JwtRequest authenticationRequest)
      throws Exception {

    authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
    final UserDetails userDetails =
        jwtInMemoryUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());

    String type = null;
    final String token = jwtTokenUtil.generateToken(userDetails);
    final List<Employee> employees;
    employees = employeeService.getEmployeeByUsername(authenticationRequest.getUsername());
    if (employees.size() > 0) {
      type = employees.get(0).getType();
    }
    return ResponseEntity.ok(new Response(token, type));
  }

  private void authenticate(final String username, final String password) throws Exception {
    Objects.requireNonNull(username);
    Objects.requireNonNull(password);
    try {
      authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(username, password));
    } catch (final DisabledException e) {
      throw new Exception("USER_DISABLED", e);
    } catch (final BadCredentialsException e) {
      throw new Exception("INVALID_CREDENTIALS", e);
    }
  }
}
