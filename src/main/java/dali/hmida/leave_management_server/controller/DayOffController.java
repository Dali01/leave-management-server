package dali.hmida.leave_management_server.controller;

import dali.hmida.leave_management_server.dto.DayOffRequestDTO;
import dali.hmida.leave_management_server.mapper.DayOffRequestMapper;
import dali.hmida.leave_management_server.model.DayOffRequest;
import dali.hmida.leave_management_server.service.DayOffRequestService;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@RestController
@CrossOrigin()
public class DayOffController {

  private final DayOffRequestService dayOffRequestService;

  public DayOffController(final DayOffRequestService dayOffRequestService) {
    this.dayOffRequestService = dayOffRequestService;
  }

  @RequestMapping(value = "/selectedDayOff", method = RequestMethod.GET)
  public List<DayOffRequest> getDayOff(
      @RequestParam(name = "date") final String date,
      @RequestParam(name = "department") final String department)
      throws ParseException {
    final SimpleDateFormat sdf = new SimpleDateFormat("dd, MMMM, yyyy", Locale.ENGLISH);
    final Date selectedDate = sdf.parse(date);

    return dayOffRequestService.getBetween(selectedDate, selectedDate, department);
  }

  @RequestMapping(value = "/wiw", method = RequestMethod.GET)
  public List<DayOffRequest> getDayOff() {
    return dayOffRequestService.getByRequester("Mohsen");
  }

  @RequestMapping(value = "/dayOffs", method = RequestMethod.GET)
  public List<DayOffRequest> getDayOffs() {
    return dayOffRequestService.getAllDayOffs();
  }

  @RequestMapping(value = "/saveDayOff", method = RequestMethod.POST)
  public DayOffRequest saveDayOff(@RequestBody final DayOffRequestDTO dayOffRequestDTO) {
    return dayOffRequestService.saveDayOffRequest(
        DayOffRequestMapper.MAPPER.fromDto(dayOffRequestDTO));
  }
}
