package dali.hmida.leave_management_server.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;

@Getter
@Setter
public class DayOffRequestDTO {
  @Id private String id;
  private String requester;
  private Integer period;
  private String status;
  private String type;
  private String startDate;
  private String endDate;

  public DayOffRequestDTO(
      final String requester,
      final Integer period,
      final String status,
      final String type,
      final String startDate,
      final String endDate) {
    this.requester = requester;
    this.period = period;
    this.status = status;
    this.type = type;
    this.startDate = startDate;
    this.endDate = endDate;
  }
}
